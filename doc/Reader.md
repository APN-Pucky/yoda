# Readers

YODA provides a few specialised readers
depending on the input formats.

## ReaderYODA

The `ReaderYODA` specialisation can be used
to read analysis objects from a stream or
an input file.
Here's an example using the output file from
the `WriterYODA` [exercise](Writer.md#writeryoda):

```cpp
vector<YODA::AnalysisObject*> aovec;
YODA::Reader& aoReader = YODA::ReaderYODA::create();
aoReader.read("output.yoda", aovec);
std::cout << "Loaded " << aovec.size() << " AOs" << std::endl;
for (const auto* ao : aovec) {
  YODA::WriterYODA::write(cout, *ao);
}
```
```
Loaded 3 AOs
BEGIN YODA_COUNTER_V3 /C
Path: /C
Title:
Type: Counter
---
# sumW       	sumW2        	numEntries
1.000000e+01 	1.000000e+02 	1.000000e+00
END YODA_COUNTER_V3

BEGIN YODA_HISTO1D_V3 /H1D_d
Path: /H1D_d
Title:
Type: Histo1D
---
# Mean: 3.470588e-01
# Integral: 1.700000e+01
Edges(A1): [0.000000e+00, 5.000000e-01, 1.000000e+00]
# sumW       	sumW2        	sumW(A1)     	sumW2(A1)    	numEntries
0.000000e+00 	0.000000e+00 	0.000000e+00 	0.000000e+00 	0.000000e+00
1.000000e+01 	1.000000e+02 	1.000000e+00 	1.000000e-01 	1.000000e+00
7.000000e+00 	4.900000e+01 	4.900000e+00 	3.430000e+00 	1.000000e+00
0.000000e+00 	0.000000e+00 	0.000000e+00 	0.000000e+00 	0.000000e+00
END YODA_HISTO1D_V3

BEGIN YODA_BINNEDHISTO<S>_V3 /H1D_s
Path: /H1D_s
Title:
Type: BinnedHisto<s>
---
# Mean: 6.250000e-01
# Integral: 8.000000e+00
Edges(A1): ["A"]
# sumW       	sumW2        	sumW(A1)     	sumW2(A1)    	numEntries
3.000000e+00 	9.000000e+00 	0.000000e+00 	0.000000e+00 	1.000000e+00
5.000000e+00 	2.500000e+01 	5.000000e+00 	5.000000e+00 	1.000000e+00
END YODA_BINNEDHISTO<S>_V3
```

Note that the `Readers` need to know in advance what sort of objects
to expect from a stream or file. By default we will preload analysis
objects only for the first 3 dimensiosn and with all permutations of
`double`, `int` and `std::string` axis edge types.
If a stream or file contains more complex objects, you can
tell the `Reader` about it ahead of loading the objects into
memory, e.g. like so:

```cpp
aoReader.addType<HistoND<4>>("Histo4D");
aoReader.addType<ScatterND<5>>("Scatter5D");
aoReader.addType<BinnedHistoND<double,std::string,unsigned int,double>>("BinnedHisto4D<d,s,j,d>");
```

## ReaderH5

In development ...

