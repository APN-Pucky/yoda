# (Binned) Storage objects

YODA's generic bin container is the `BinnedStorage` class. It is templated on
* the bin content type
* the axis edge types.

A simple example using `double` as the bin content type
using the `Binning` object defined [here](Binning.md):

```cpp
// BinnedStorage<ContentType, AxisType1, AxisType2, ...>
BinnedStorage<std::string, double, int, std::string> bs(binning);
```

The dimensionality of the `BinnedStorage` is as follows:
```cpp
std::cout << "This BinnedStorage has dimension " << bs.dim();
std::cout << " which is 1 for the content and " << bs.binDim();
std::cout << " for the axes." << std::endl;
```

```
This BinnedStorage has dimension 4 which is 1 for the content and 3 for the axes.
```

By default the `BinnedStorage` assumes you only want to now about visible bins,
i.e. excluding under-/over-/otherflow bins:

```cpp
std::cout << "The number of visible bins is " << bs.numBins();
std::cout << std::endl;
std::cout << "The number of bins including overflows is " << bs.numBins(true);
std::cout << std::endl;
```

```
The number of visible bins is 42
The number of bins including overflows is 128
```

**The `BinnedStorage` is great for holding statistically inert quantities,
i.e. things that can no longer be incremented,
e.g. a measured differential cross-section.**

If we want a live object where the bins can still be incremented,
we need a fill adapater - check out the following subsection.

## FillableStorage: the fillable BinnedStorage

The `FillableStorage` class is derived from the
`BinnedStorage` class and introduces a new
feature: the fill method.

The fill method takes translates the coordinates
along each of a given set of axes into the global
bin index and calls the "fill adapter" for the
corresponding bin. The purpose of the "fill adapter"
is to take care of the details of how the bin content
should be incremented. This might be as trivial as
calling the `+=` operator with the event weight on
the bin content or it could be more sophisticated.

Note that the number of fill dimensions can in general
be different from the number of binned dimensions
(e.g. consider a profile which is filled in `N+1` dimensions,
only `N` of which are binned). For this reason,
the fill dimensionality is also one of the template
arguments of the `FillableStorage` object.

Here's an example with content type `double`, binned and
filled along an axis of edge type `string`:

```cpp
FillableStorage<1, double, std::string> fs (Axis<std::string>({"A", "B", "C"}));
std::cout << "This FillableStorage has fill dimension " << fs.fillDim() << std::endl;
std::cout << "The number of visible bins is " << fs.numBins();
std::cout << std::endl;
std::cout << "The number of bins including overflows is " << fs.numBins(true);
std::cout << std::endl;
```

```
This FillableStorage has fill dimension 1
The number of visible bins is 3
The number of bins including overflows is 4
```

It can be filled by providing a tuple of coordinates:
```cpp
fs.fill({"A"});
fs.fill({"B"},           10); // optional weight
fs.fill({"C"},          100); // optional weight
fs.fill({"not an edge"}, -1); // "other"flow
for (const auto& b : fs.bins(true)) {
  std::cout << "Bin " << b.index() << " has content " << b.raw() << std::endl;
}
```

```
Bin 0 has content -1
Bin 1 has content 1
Bin 2 has content 10
Bin 3 has content 100
```

There are also template specialistions for bin contents of type
`Dbn<N>` and `Estimate` available. Please see the dedicated
documentation forthe  [BinnedDbn](BinnedDbn.md) and
[BinnedEstimate](BinnedEstimate.md) classes.

## NaN treatment

See the discussion [here](BinnedDbn.md#nan-treatment).

