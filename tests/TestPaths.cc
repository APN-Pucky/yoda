#include "YODA/Utils/Paths.h"
#include "YODA/Utils/MetaUtils.h"
#include "YODA/Utils/StringUtils.h"
#include <iostream>
#include <string>

using namespace YODA;

auto testPaths() {

  const std::string libPath = getLibPath();
  if ( !Utils::endswith(libPath, "/lib") )  return CHECK_TEST_RES(false);

  const std::string dataPath = getDataPath();
  if ( !Utils::endswith(dataPath, "/share/YODA") )  return CHECK_TEST_RES(false);

  const std::vector<std::string> yodaDataPaths = getYodaDataPath();
  if ( yodaDataPaths.size() != 2 )  return CHECK_TEST_RES(false);

  bool hasShare = false;
  for (const std::string& p : yodaDataPaths) {
    if ( Utils::endswith(p, "/share/YODA") )  hasShare = true;
  }
  if ( !hasShare )  return CHECK_TEST_RES(false);

  return CHECK_TEST_RES(true);
}

int main() {
  return CHECK_TEST_RES(testPaths() == EXIT_SUCCESS);
}
