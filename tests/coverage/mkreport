#! /usr/bin/env python

import os, json, subprocess

BASE = os.path.dirname(os.path.realpath(__file__))

def getVal(item):
    item = item[ item.find('>')+1 : ]
    return item[ : item.find('<') ].replace('&nbsp;', '')

def getDir(item):
    item = item[ : item.find('</a>') ]
    item = item[ item.rfind('>')+1 : ]
    if 'yoda' in item:
        item = item[ item.find('yoda') : ]
    if item.endswith('/'):
        item = item[:-1]
    return '`'+item+'`'

IS_LLVM = False
REPORT = { }
with open(BASE + "/html/index.html", "r") as f:
  header = '**Total**'
  counts = []
  for line in f.readlines():
      line = line.strip()
      if 'coverDirectory' in line:
          header = getDir(line)
          IS_LLVM = True
      if 'coverFile' in line:
          header = getDir(line)
      if 'headerCovTableEntry"' in line:
          counts.append( getVal(line) )
      if 'coverNum' in line:
          for val in getVal(line).split(' / '):
              counts.append( val )
      if len(counts) == 4:
          REPORT[header] = counts
          counts = []

REF = {}
with open(BASE+'/coverage.json') as f:
    REF = json.load(f)

NEW_REF = {}
MSG  = r"\r\n\r\n# :chart_with_upwards_trend: - Coverage Report\r\n\r\n"
MSG += r"| | Lines | Functions | &Delta;(new,old) |\r\n| :--- | :---: | :---: | :--- |\r\n"
for k,(lhit, ltot, fhit, ftot) in REPORT.items():
    lref, fref = REF.get(k, [-1,-1])
    if IS_LLVM:
        lhit, ltot, fhit, ftot = ltot, lhit, ftot, fhit
    lval = 100*float(lhit)/float(ltot)
    fval = 100*float(fhit)/float(ftot)
    NEW_REF[k] = [round(lval,1), round(fval,1)]
    delta = ':no_entry_sign:'
    if lref >= 0.:
        diff = round(10*lval) - round(10*lref)
        if round(lref,1) == 1000 and round(lval,1) == 1000:
            delta = ':green_heart: (stable)' # ideal scenario
        elif diff > 0:  delta = f':green_heart: ({0.1*diff:+.1f} %)'
        elif diff < 0:  delta = f':broken_heart: ({0.1*diff:+.1f} %)'
        else:           delta = ':yellow_heart: (stable)'
    if 'Total' in k:
        MSG += "| " + k + " | **" + f"{lval:.1f} %** | **{fval:.1f} %** | " + delta + r" |\r\n"
    else:
        MSG += "| " + k + " | " + f"{lval:.1f} % | {fval:.1f} % | " + delta + r" |\r\n"
MSG += r"\r\n"

with open(BASE+'/coverage.json', 'w') as f:
    json.dump(NEW_REF, f, indent=2)

print(MSG)

