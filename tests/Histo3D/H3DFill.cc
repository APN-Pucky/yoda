#include "YODA/Histo.h"
#include "YODA/Scatter.h"
#include "YODA/Utils/MathUtils.h"

#include <cmath>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <sys/time.h>
using namespace std;
using namespace YODA;

int main() {
  ios_base::sync_with_stdio(0);
  Histo3D h(200, 0, 100, 200, 0, 100, 200, 0, 100);
  const size_t nBins = h.numBins();
  cout << "nBins = " << nBins << endl;
  if (nBins != 8000000) {
    cout << "FAIL" << endl;
    return -1;
  }

  cout << "Testing fill operation:                  ";
  size_t idx = h.indexAt(16.0123, 12.213, 45.6);
  int out = h.fill(16.0123, 12.213, 45.6, 2);
  if (out != int(idx)) {
    cout << "FAIL" << endl;
    return -1;
  }

  h.maskBin(h.indexAt(12.3,32.1,45.6));
  if (h.numBins() != (nBins-1)) {
    cout << "FAIL" << endl;
    return -1;
  }
  cout << "PASS" << endl;

  cout << "Checking fill with coord group:          ";
  h.fill(Histo3D::FillType{50.,50.,50.}, 10);
  if (h.binAt(50.,50.,50.).sumW() != 10) {
    cout << "FAIL" << endl;
    return -1;
  }
  cout << "PASS" << endl;

  return EXIT_SUCCESS;
}
