#include "YODA/Counter.h"
#include "YODA/Estimate0D.h"
#include "YODA/Scatter.h"
#include "YODA/Utils/MathUtils.h"
#include "YODA/Utils/MetaUtils.h"
#include <cmath>
#include <vector>

using namespace YODA;

auto testCounter() {
  Counter c1;

  const size_t N = 10000;
  for (size_t i=0; i<N; ++i)  c1.fill(10.0);

  if (c1.sumW() != 10*N || c1.numEntries() != N)  return CHECK_TEST_RES(false);

  Counter c2 = c1.clone();
  if (c2.sumW() != 10*N || c2.numEntries() != N)  return CHECK_TEST_RES(false);

  c2.scaleW(2.0);
  if (c2.sumW() != 20*N || c2.numEntries() != N)  return CHECK_TEST_RES(false);

  Counter* c3 = c2.newclone();
  if (c3->sumW() != 20*N || c3->numEntries() != N)  return CHECK_TEST_RES(false);

  Estimate0D e1 = c1 / c2;
  if (e1.val() != 0.5)  return CHECK_TEST_RES(false);

  Estimate0D e2 = efficiency(c1, c2);
  if (e2.val() != 0.5)  return CHECK_TEST_RES(false);

  S1D s1 = c2.mkScatter();
  std::vector<double> xvals = s1.xVals();
  if (xvals.size() != 1 || xvals[0] != 20*N)  return CHECK_TEST_RES(false);

  return CHECK_TEST_RES(true);
}

int main() {
  return CHECK_TEST_RES(testCounter() == EXIT_SUCCESS);
}
