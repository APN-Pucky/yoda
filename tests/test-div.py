#! /usr/bin/env python3

import yoda, random

h1, h2 = [yoda.Histo1D(4, 0, 10) for _ in range(2)]
for i in range(1000):
    h1.fill(random.uniform(0,10))
    h2.fill(random.uniform(0,10))
e = h1 / h2
print(e)

for b in e.bins():
    print(" ", b)

print()

e = h1.divideBy(h2)
print(e)
for b in e.bins():
    print(" ", b)
