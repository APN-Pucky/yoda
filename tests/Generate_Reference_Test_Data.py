import numpy as np

#mu = 2
#sigma = 3

#s = np.random.normal(mu, sigma, 1000)
#np.savetxt("normalDistr.data", s, fmt='%f')

s = np.loadtxt("normalDistr.txt")

def printSampleStatistics(s):	
	print(np.mean(s))
	print(np.std(s, ddof=1))
	print(np.var(s, ddof=1))
	print(np.sqrt(np.mean(np.square(s)))) 
	print(np.std(s, ddof=1)/np.sqrt(len(s)))

printSampleStatistics(s)