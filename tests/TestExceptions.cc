#include "YODA/Histo.h"
#include "YODA/IO.h"
#include "YODA/WriterYODA.h"
#include "YODA/Utils/MathUtils.h"
#include "YODA/Utils/MetaUtils.h"

#include <cmath>
#include <iostream>
#include <vector>

using namespace YODA;

auto testExceptions() {

  bool pass = false;
  try {
    Axis<double> a;
    a.index(0);
  }
  catch (const BinningError&) {
    pass = true;
  }
  if (!pass)  return CHECK_TEST_RES(false);

  pass = false;
  try {
    Axis<int> a;
    a.edge(1);
  }
  catch (const RangeError&) {
    pass = true;
  }
  if (!pass)  return CHECK_TEST_RES(false);

  pass = false;
  try {
    Histo1D h(10, 0.0, 1.0);
    h.normalize();
  }
  catch (const WeightError&) {
    pass = true;
  }
  if (!pass)  return CHECK_TEST_RES(false);

  pass = false;
  try {
    Counter c;
    c.annotation("MISSING");
  }
  catch (const AnnotationError&) {
    pass = true;
  }
  if (!pass)  return CHECK_TEST_RES(false);

  pass = false;
  try {
    std::vector<AnalysisObject*> aos1 = YODA::read("brokenfile.yoda");
  }
  catch (const ReadError&) {
    pass = true;
  }
  if (!pass)  return CHECK_TEST_RES(false);

  pass = false;
  try {
    Counter num, den;
    num.fill(); num.fill();
    den.fill();
    efficiency(num, den);
  }
  catch (const UserError&) {
    pass = true;
  }
  if (!pass)  return CHECK_TEST_RES(false);

  return CHECK_TEST_RES(true);
}

int main() {
  return CHECK_TEST_RES(testExceptions() == EXIT_SUCCESS);
}
