#include "YODA/Profile.h"
#include "YODA/Utils/Formatting.h"

using namespace YODA;
using namespace std;

int main() {
  MSG_BLUE("Testing Profile1D construction: ");

  MSG_(PAD(70) << "Testing range/number constructor ");
  Profile2D p1(100, 0, 100, 100, 0, 100);
  if (p1.sumW() != 0 || p1.sumW2() != 0 || p1.sumW(false) != 0 || p1.sumW2(false) != 0) {
    MSG_RED("FAIL");
    return -1;
  }
  MSG_GREEN("PASS");


  MSG_(PAD(70) << "Testing explicit edges constructor ");
  vector<double> edges;
  for (size_t i = 0; i < 101; ++i) edges.push_back(i);
  Profile2D p2(edges, edges);
  if (p2.sumW() != 0 || p2.sumW2() != 0 || p2.sumW(false) != 0 || p2.sumW2(false) != 0) {
    MSG_RED("FAIL");
    return -1;
  }
  if (p2.numBins() != 10000){
    MSG_RED("FAIL");
    return -1;
  }
  MSG_GREEN("PASS");

  MSG_(PAD(70) << "Filling using coordinate group ");
  p1.fill(Profile2D::FillType{50.,50.,50.}, 10);
  if (p1.binAt(50.,50.).sumW() != 10){
    MSG_RED("FAIL");
    return -1;
  }
  MSG_GREEN("PASS");

  MSG_(PAD(70) << "Preliminary testing of = operator ");
  Profile2D p3(edges, edges);
  p3 = p2;
  if (p3.sumW() != 0 || p3.sumW2() != 0 || p3.sumW(false) != 0 || p3.sumW2(false) != 0){
    MSG_RED("FAIL");
    return -1;
  }
  if (p3.numBins() != 10000){
    MSG_RED("FAIL");
    return -1;
  }
  MSG_GREEN("PASS");

  MSG_(PAD(70) << "Testing of newclone ");
  Profile2D* pptr = p3.newclone();
  if (pptr == nullptr) {
    MSG_RED("FAIL");
    return -1;
  }
  MSG_GREEN("PASS");

  return EXIT_SUCCESS;
}
