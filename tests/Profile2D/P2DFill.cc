#include "YODA/Profile.h"
#include "YODA/Utils/Formatting.h"

using namespace YODA;
using namespace std;

int main() {
  MSG_BLUE("Testing Profile1D filling: ");

  MSG_(PAD(70) << "Setting up 100-bin profile histo ");
  Profile2D p(100, 0, 100, 100, 0, 100);
  MSG_GREEN("PASS");

  MSG_(PAD(70) << "Testing fill operator: ");
  p.fill(1,1,1,2);
  if (p.sumW() != 2 || p.sumW2() != 4) {
    MSG_RED("FAIL");
    return -1;
  }
  MSG_GREEN("PASS");


  MSG_(PAD(70) << "Testing the fill of the underflow: ");
  p.fill(-10, -10, 2, 3);
  if (!fuzzyEquals(p.bin(0).sumW(), 3)) {
    MSG_RED("FAIL");
    return -1;
  }
  MSG_GREEN("PASS");


  MSG_(PAD(70) << "Testing the fill of the overflow: ");
  p.fill(110,110,2,3);
  if (!fuzzyEquals(p.bin(p.numBins(true)-1).sumW(), 3)) {
    MSG_RED("FAIL");
    return -1;
  }
  MSG_GREEN("PASS");

  return EXIT_SUCCESS;
}
