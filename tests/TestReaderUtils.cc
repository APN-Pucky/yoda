#include "YODA/Histo.h"
#include "YODA/ReaderYODA.h"
#include "YODA/IO.h"
#include <iostream>

using namespace std;
using namespace YODA;

int main() {

  #ifdef HAVE_LIBZ
  vector<AnalysisObject*> aos = YODA::read("yodamixed.yoda.gz");
  cout << "Loaded " << aos.size() << " AnalysisObjects!" << endl;
  if (aos.size() != 16)  return -1;
  #endif

  return EXIT_SUCCESS;
}
