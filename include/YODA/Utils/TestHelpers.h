#ifndef TEST_HELPERS_H
#define TEST_HELPERS_H

#include <vector>
#include <fstream>

namespace TestHelpers {

  /// From here: https://stackoverflow.com/a/2581839
  void skip_lines(std::istream& pStream, size_t pLines)
  {
      std::string s;
      for (; pLines; --pLines)
          std::getline(pStream, s);
  }

  /// @brief Precalculated data with it's properties used in statistical objects tests.
  struct TestData {
    TestData() {
        std::ifstream infile("normalDistr.data");

        data.reserve(1000);

        skip_lines(infile, 5);

        infile >> meanVal;
        infile >> stdDevVal;
        infile >> varVal;
        infile >> RMSVal;
        infile >> stdErrVal;

        double buffer = 0;

        while(infile >> buffer)
          data.push_back(buffer);
    }

    double meanVal;
    double stdDevVal;
    double stdErrVal;
    double RMSVal;
    double varVal;

    std::vector<double> data;
  };
}

#endif
